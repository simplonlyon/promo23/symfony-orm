<?php

namespace App\DataFixtures;

use App\Entity\Bike;
use App\Entity\Rental;
use App\Entity\Station;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $stations = [];



        for ($i=0; $i < 10; $i++) { 
            
            $station = new Station();
            $station->setLabel($faker->streetName());
            $station->setSlots($faker->numberBetween(5, 30));
            $station->setLatitude($faker->latitude(45.70, 45.80));
            $station->setLongitude($faker->longitude(4.79, 4.91));
            $manager->persist($station);
            $stations[] = $station;

            for ($j=0; $j < $faker->numberBetween(0, $station->getSlots()); $j++) { 
                
                $bike = new Bike();
                $bike->setElectric($faker->boolean());
                $bike->setBroken($faker->boolean(25));
                $bike->setStation($station);
                $manager->persist($bike);
                
                for ($k=0; $k < $faker->numberBetween(0,5); $k++) { 
                    $rental = new Rental();
                    //On récupère un user créé par une autre fixture via le getReference
                    $rental->setUser($this->getReference(UserFixtures::USER_REFERENCE.$faker->numberBetween(0, 9)));
                    $rental->setBike($bike);
                    $rental->setStartStation($stations[$faker->numberBetween(0, count($stations)-1)]);
                    $rental->setStartTime($faker->dateTimeThisMonth());
                    if($faker->boolean(90)) {
                        $rental->setEndTime($faker->dateTimeBetween($rental->getStartTime()));
                        $rental->setReturnStation($stations[$faker->numberBetween(0, count($stations)-1)]);
                    }
                    $manager->persist($rental);

                }
            }


        }

        $manager->flush();
    }
	/**
	 * This method must return an array of fixtures classes
	 * on which the implementing class depends on
	 * @return array<string>
	 */
	public function getDependencies() {
        //Indique les fixtures qui devront se lancer avant celle ci
        return [
            UserFixtures::class
        ];
	}
}
