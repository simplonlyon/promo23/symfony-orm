<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class UserFixtures extends Fixture
{
    const USER_REFERENCE = 'user';

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        $user = new User();
        $user->setEmail('test@test.com');
        $user->setPassword('$2y$13$YU4HXid7nHvTJdZCewkuqeq3L83vYAI8KcLr2IBiJ.q1yBTu6JSTK');//1234
        $user->setRole('ROLE_USER');
        $manager->persist($user);

        $user = new User();
        $user->setEmail('admin@admin.com');
        $user->setPassword('$2y$13$YU4HXid7nHvTJdZCewkuqeq3L83vYAI8KcLr2IBiJ.q1yBTu6JSTK');//1234
        $user->setRole('ROLE_ADMIN');
        $manager->persist($user);
        
        for ($i=0; $i < 10; $i++) { 
            
            $user = new User();
            $user->setEmail($faker->email());
            $user->setPassword('$2y$13$YU4HXid7nHvTJdZCewkuqeq3L83vYAI8KcLr2IBiJ.q1yBTu6JSTK');//1234
            $user->setRole('ROLE_USER');
            $manager->persist($user);
            //On stock une référence au user qui pourra être utilisé dans d'autres fixtures
            $this->addReference(self::USER_REFERENCE.$i, $user);
            
        }
        $manager->flush();
    }
}
