<?php

namespace App\Controller;

use App\Entity\Bike;
use App\Repository\BikeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/bike')]
class BikeController extends AbstractController
{
    public function __construct(private BikeRepository $repo) {
    }
    #[Route(methods:'POST')]
    public function add( Request $request, SerializerInterface $serializer, EntityManagerInterface $em) {

        //pour l'instant, ça marche pas avec une station dedans
        $bike = $serializer->deserialize($request->getContent(), Bike::class, 'json');

        $this->repo->save($bike, true);

        

        return $this->json($bike, Response::HTTP_CREATED);
    }
}
