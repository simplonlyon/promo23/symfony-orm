<?php

namespace App\Controller;

use App\Entity\Bike;
use App\Entity\Rental;
use App\Entity\Station;
use App\Repository\RentalRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/api/rental')]
class RentalController extends AbstractController
{
    #[Route('/{id}', methods: 'POST')]
    public function rentBike(Bike $bike, RentalRepository $repo): Response
    {
        if(!$bike->getStation() || $bike->isBroken()) {
            return $this->json(['error' => 'Cannot rent broken or unavailable bike'], Response::HTTP_BAD_REQUEST);
        }
        if($repo->findOneBy(['user' => $this->getUser(), 'endTime' => null])) {
            return $this->json(['error' => 'Already renting a bike'], Response::HTTP_BAD_REQUEST);
        }
        
        $rental = new Rental();
        $rental->setBike($bike);
        $rental->setUser($this->getUser());
        $rental->setStartTime(new \DateTime());
        $rental->setStartStation($bike->getStation());
        $bike->setStation(null);
        $repo->save($rental, true);
        
        return $this->json($rental);
    }

    #[Route(methods: 'GET')]
    public function rentalHistory(RentalRepository $repo) {
        $rentals = $repo->findHistory($this->getUser());
        // $rentals = $repo->findBy(['user' => $this->getUser()]);

        return $this->json($rentals);
    }

    #[Route('/current', methods: 'GET')]
    public function currentRental(RentalRepository $repo) {
        // $rentals = $repo->findHistory($this->getUser());
        $rental = $repo->findOneBy(['user' => $this->getUser(), 'endTime' => null]);
        if(!$rental) {
            return $this->json(['error' => 'No Current Rental'], Response::HTTP_NOT_FOUND);
        }

        return $this->json($rental);
    }

    #[Route('/{id}', methods: 'PATCH')]
    public function endLocation(Station $station, RentalRepository $repo) {
        $rental = $repo->findOneBy(['user' => $this->getUser(), 'endTime' => null]);
        if(!$rental) {
            return $this->json(['error' => 'No Current Rental'], Response::HTTP_NOT_FOUND);
        }

        $rental->setEndTime(new \DateTime());
        $rental->setReturnStation($station);
        $rental->getBike()->setStation($station);
        $repo->save($rental, true);
        return $this->json($rental);
    }
}
