<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230220124834 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE bike (id INT AUTO_INCREMENT NOT NULL, station_id INT DEFAULT NULL, electric TINYINT(1) NOT NULL, broken TINYINT(1) NOT NULL, INDEX IDX_4CBC378021BDB235 (station_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rental (id INT AUTO_INCREMENT NOT NULL, bike_id INT DEFAULT NULL, start_station_id INT NOT NULL, return_station_id INT DEFAULT NULL, end_time DATETIME DEFAULT NULL, start_time DATETIME NOT NULL, user VARCHAR(255) NOT NULL, INDEX IDX_1619C27DD5A4816F (bike_id), INDEX IDX_1619C27D53721DCB (start_station_id), INDEX IDX_1619C27DEA291807 (return_station_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE station (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, slots INT NOT NULL, latitude DOUBLE PRECISION DEFAULT NULL, longitude DOUBLE PRECISION DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE bike ADD CONSTRAINT FK_4CBC378021BDB235 FOREIGN KEY (station_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE rental ADD CONSTRAINT FK_1619C27DD5A4816F FOREIGN KEY (bike_id) REFERENCES bike (id)');
        $this->addSql('ALTER TABLE rental ADD CONSTRAINT FK_1619C27D53721DCB FOREIGN KEY (start_station_id) REFERENCES station (id)');
        $this->addSql('ALTER TABLE rental ADD CONSTRAINT FK_1619C27DEA291807 FOREIGN KEY (return_station_id) REFERENCES station (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE bike DROP FOREIGN KEY FK_4CBC378021BDB235');
        $this->addSql('ALTER TABLE rental DROP FOREIGN KEY FK_1619C27DD5A4816F');
        $this->addSql('ALTER TABLE rental DROP FOREIGN KEY FK_1619C27D53721DCB');
        $this->addSql('ALTER TABLE rental DROP FOREIGN KEY FK_1619C27DEA291807');
        $this->addSql('DROP TABLE bike');
        $this->addSql('DROP TABLE rental');
        $this->addSql('DROP TABLE station');
    }
}
