# Symfony ORM
Un projet Symfony avec Doctrine

## How To Use 
1. Créer un fichier `.env.local` et mettre dedans la string de connexion à la base de données sous ce format : `DATABASE_URL="mysql://username:password@127.0.0.1:3306/database_name?serverVersion=mariadb-10.3.37&charset=utf8mb4"` (en remplaçant username/password/database_name par les info qui vont bien, et éventuellement le serverVersion également si jamais vous n'êtes pas sur mariadb)
2. Créer la base de données si ce n'est pas fait `bin/console doctrine:database:create` 
3. Exécuter les migrations : `bin/console doctrine:migrations:migrate`
4. Créer les clés privées et publiques : `bin/console lexik:jwt:generate-keypair`
5. Lancer le serveur `symfony server:start`

## Les commandes principales et quand les utiliser

`bin/console make:entity` (ou ma:en) : pour créer une entité via le cli avec ses propriétés et ses relations avec les autres entités (crée aussi les repositories). Peut également être utilisée pour modifier une entité existante.

`bin/console make:migration` (ou ma:mi) : Pour créer un fichier de migration qui contiendra les requête SQL à exécuter pour passer de l'état actuel de la base de données à ce qu'on a définit dans nos entités. Je conseil de faire une migration une première fois quand on a définit toutes nos entités, puis d'en refaire seulement si en cours de projet on se rend compte qu'on a oublié des choses ou qu'on doit modifier notre structure de bdd

`bin/console doctrine:migrations:migrate` (ou do:mi:mi) : Exécute toutes les migrations du projet, à exécuter quand on clone ou qu'on pull un projet, ou après avoir créer une migration avec ma:mi

`bin/console doctrine:database:create` (ou do:da:cr) : Crée la base de données en se basant sur la DATABASE_URL

`bin/console doctrine:database:drop` (ou do:da:dr) : Drop la base de données

### Workflow possible
1. Créer le .env.local avec mon DATABASE_URL
2. Créer la bdd avec do:da:cr
3. Créer les différentes entités avec leurs propriétés propres avec ma:en
4. Modifier les entités pour rajouter les relations (à nouveau avec ma:en en choisissant les entités existantes auxquelles on veut rajouter des relations)
5. Créer une migration avec ma:mi
6. Exécuter la migration avec do:mi:mi
7. Organiser un grand square dance pour fêter la création de notre couche data

## L'Authentification

### Création de l'entité
Dépendance nécessaire (si pas déjà installée) : `composer req security` 

On commence par créer une entité qui servira de user. Elle peut s'appeler comme on le souhaite, avoir n'importe quelles propriétés, pour être utilisée comme user il faut simplement qu'elle implémente les interface `UserInterface` et `PasswordAuthenticatedUserInterface`

```php

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    //...

    /**
     * Méthode indiquant à Symfony Security comment récupérer le password (au cas où la propriété ne
     * s'appelerait pas password dans l'entité)
     */
    public function getPassword() {
        return $this->password;
    }
    /**
     * Méthode indiquant les rôles du User, si on a qu'un seul rôle, on peut faire en sorte de return
     * directement ['ROLE_USER'] par exemple, mais si on a plusieurs rôles mais un seul simultané, on peut
     * faire comme ici.
     */
    public function getRoles(): array {
        return [$this->role];
	}
	/**
     * Méthode qui sert à remettre à zéro les données sensibles dans l'entité pour ne pas les persistées
     * (par exemple si on avait un champ pour le mot de passe en clair différent du champ password)
     */
	public function eraseCredentials() {
	}
	/**
     * Méthode indiquant à Symfony Security l'identifiant du user, donc soit le mail, soit le username
     * soit le téléphone, soit le numéro de sécu etc.
     */
	public function getUserIdentifier(): string {
        return $this->email;
	}
```

Dans le fichier `config/packages/security.yaml`, on rajoute un provider indiquant à symfony comment récupérer le user

```yaml
security:
    # ...
    providers:
        app_user_provider:
            entity:
                class: App\Entity\User
                property: email
```

### Route d'inscription
On crée une route d'inscription dans un contrôleur dans laquelle il faudra : 
* Récupérer les données d'inscription obligatoire (au minimum identifiant et mot de passe) et les valider
* Vérifier qu'un user avec l'identifiant donné n'existe pas déjà
* Hasher le mot de passe et l'assigner à l'instance de l'entité
* Assigner les valeurs par défaut nécessaire (par exemple le rôle par défaut, un panier vide si besoin, la date d'inscription, etc.)
* Faire persister le user
Exemple de ces différentes étapes [ici dans la méthode register()](src/Controller/AuthController.php)

### Authentification par JWT
Dépendance nécessaire (si pas déjà installée) : `composer req jwt` 

Dans le cadre d'une application client-serveur, une des manières d'authentification les plus utilisées est celle par JWT. Le flow d'authentification est le suivant :
1. On fait une requête avec nos credentials (email/password par exemple) vers une route de login. Le serveur vérifie si un user avec cet identifiant existe, si oui on récupère le password hashé stocké en base de donnée, on utilise ses information pour hasher le mot de passe de login de la même manière, si les deux hash correspondent, c'est correct, on crée un JWT renvoyé au client
2. Le client stock le JWT quelque part (en localStorage par exemple). Toute nouvelle requête contiendra le JWT dans ses Authorization Headers
3. Lorsque le serveur reçoit une requête avec un JWT, il vérifie la validité de celui ci (est-ce qu'il a été altéré, sa signature est-elle ok, est-il expiré)
4. Si le token est valide, Symfony récupère son contenu et s'en sert pour récupérer en base de données l'entité user correspondante à ce token
5. Symfony vérifie si le user récupéré a accès à la ressource demandée, vis à vis de ce qui a été défini par exemple dans les access_control du security.yaml

#### Configuration de l'extension JWT
Dans le fichier de configuration `config/packages/security.yaml`, on rajoute :
```yaml
security:
    #...    
    firewalls:
        #...
        login:
            pattern: ^/api/login
            stateless: true
            json_login:
                check_path: /api/login
                success_handler: lexik_jwt_authentication.handler.authentication_success
                failure_handler: lexik_jwt_authentication.handler.authentication_failure
                username_path: email
        api:
            pattern:   ^/api
            stateless: true
            jwt: ~

```

La partie login contien dans le check_path la route qui permettra de s'authentifier, on peut la modifier, il faut juste que le check_path corresponde au pattern.

Dans la partie login, on indique quelles url de notre serveur utiliseront l'authentification par JWT, ici ça sera toutes les routes commençant par /api (c'est techniquement modifiable)


Dans le fichier de configuration `config/routes.yaml`, on rajoute :
```yaml
api_login_check:
    path: /api/login
```
Où le path devra correspondre au check_path indiqué dans le security.yaml

Ensuite on génère une paire de clés privée/publique avec `bin/console lexik:jwt:generate-keypair`

Dans le fichier `config/packages/lexik_jwt_authentication.yaml` on peut rajouter un `token_ttl: 100000000000` par exemple pour créer un token avec une date d'expiration très lointaine pour les tests (à retirer en prod)

#### Utilisation de l'authentification JWT avec ThunderClient
1. On fait une requête en POST vers `http://localhost:8000/api/login` en indiquant dans le body 
   ```json
    {
        "email":"identifiant@mail.com",
        "password":"leMotDePasse"
    }
    ```
2. On copie le token renvoyé
3. Pour chaque nouvelle requête, on rajoute dans l'onglet Auth > Bearer le token copié


### Sécurisation des routes
Pour sécuriser les routes, on peut rajouter dans le `config/packages/security.yaml`, dans la partie `access_control` les routes accessibles ou non

```yaml
security:
    enable_authenticator_manager: true
    #...
    access_control:
        - { path: ^/api/admin, roles: ROLE_ADMIN }
        - { path: /api/station, roles: PUBLIC_ACCESS, methods:[GET] }
        - { path: /api/station, roles: ROLE_USER }
```
Ici, toutes les routes commençant par /api/admin ne seront accessibles que par les user avec un rôle ROLE_ADMIN, la route /api/station en GET sera accessible par n'importe qui, toutes les autres méthodes de la route /api/station ne seront accessible que par les user avec le rôle ROLE_USER